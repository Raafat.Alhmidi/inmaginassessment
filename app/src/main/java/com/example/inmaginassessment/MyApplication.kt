package com.example.inmaginassessment

import android.app.Application

/**
 * Created by Raafat Alhmidi on 5/25/20 @7:49 PM.
 */
class MyApplication: Application() {
    companion object {
        lateinit var instance: MyApplication
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
    }
}