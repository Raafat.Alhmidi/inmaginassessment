package com.example.inmaginassessment

import android.net.Uri
import android.os.Bundle
import android.text.Html
import android.text.method.LinkMovementMethod
import android.view.View
import android.widget.AdapterView
import android.widget.SeekBar
import android.widget.SeekBar.OnSeekBarChangeListener
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.example.inmaginassessment.databinding.ActivityMainBinding
import com.example.inmaginassessment.ui.filtersDialog.FiltersDialogFragment
import com.example.inmaginassessment.ui.filtersDialog.OnDoneClickListner
import com.example.inmaginassessment.ui.settingsDialog.SettingsDialog
import com.example.inmaginassessment.ui.settingsDialog.SettingsViewModel
import com.example.inmaginassessment.utils.Cell
import com.example.inmaginassessment.utils.OnCellClickListner
import com.example.inmaginassessment.utils.ScaleType
import com.kroegerama.imgpicker.BottomSheetImagePicker
import com.kroegerama.imgpicker.ButtonType
import kotlin.math.sign

class MainActivity : AppCompatActivity(), BottomSheetImagePicker.OnImagesSelectedListener {
    private lateinit var binding: ActivityMainBinding
    private val SELECT_IMAGE_TAG = "SELECT_IMAGE"
    private var selectedCell: Cell? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        with(binding) {

            canvasView.onCellClickListener = object : OnCellClickListner {
                override fun onClick(cell: Cell?) {
                    selectedCell = cell
                    if (cell?.bitmap == null)
                        BottomSheetImagePicker.Builder(BuildConfig.APPLICATION_ID)
                            .cameraButton(ButtonType.Button)
                            .galleryButton(ButtonType.Button)
                            .requestTag(SELECT_IMAGE_TAG)
                            .show(supportFragmentManager, null)
                    else {
                        FiltersDialogFragment(cell,object :OnDoneClickListner{
                            override fun onClick(cell: Cell?) {
                                if (cell != null) {
                                    canvasView.drawImagOnCell(cell.bitmap,cell.i,cell.j)
                                }
                            }

                        }).show(supportFragmentManager,"Filters")
                    }

                }
            }

            if (canvasView.rows <= 10) {
                SettingsViewModel.instance?.rowsLD?.value = canvasView.rows
            }
            if (canvasView.cols <= 10) {
                SettingsViewModel.instance?.colsLD?.value = canvasView.cols
            }
            val padding  = pxToDp(canvasView.cellsPadding,this@MainActivity).toInt()
            if (padding <= 100) {
                SettingsViewModel.instance?.paddingLD?.value = padding

            }

            settingsFab.setOnClickListener{
                SettingsDialog().show(supportFragmentManager,"Settings")
                observeSettingsVM()
            }


        }



    }

    private fun observeSettingsVM(){
        SettingsViewModel.instance?.colsLD?.observe(this, androidx.lifecycle.Observer  {
            binding.canvasView.cols = it
        })

        SettingsViewModel.instance?.rowsLD?.observe(this, androidx.lifecycle.Observer  {
            binding.canvasView.rows = it
        })

        SettingsViewModel.instance?.paddingLD?.observe(this, androidx.lifecycle.Observer  {
            binding.canvasView.cellsPadding = dpToPx(it.toFloat(),this@MainActivity)
        })

        SettingsViewModel.instance?.scaleTypeLD?.observe(this, androidx.lifecycle.Observer  {
            binding.canvasView.scaleType = ScaleType.values()[it]
        })
    }

    override fun onImagesSelected(uris: List<Uri>, tag: String?) {
        selectedCell?.apply {
            binding.canvasView.drawImagOnCell(uris.first(), i, j)
        }
    }
}
