package com.example.inmaginassessment.utils

import android.content.Context
import android.graphics.*
import android.net.Uri
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import android.widget.Toast
import androidx.core.content.withStyledAttributes
import com.example.inmaginassessment.R
import com.example.inmaginassessment.decodeUriToBitmap
import com.example.inmaginassessment.dpToPx
import com.example.inmaginassessment.model.MyFilter
import com.example.inmaginassessment.spToPx


/**
 * Created by Raafat Alhmidi on 5/21/20 @5:12 AM.
 */

enum class ScaleType {
    CENTER_CROP,
    CENTER_INSIDE,
    CENTER,
    FIT_XY,
    FIT_END,
    FIT_START
}

data class Cell(
    val i: Int,
    val j: Int,
    var centerX: Float,
    var centerY: Float
) {
    var bitmap: Bitmap? = null
    var orgBitmap: Bitmap? = null
    var uri: Uri? = null
    var myFilter: MyFilter? = null
    override fun toString() = "($i, $j)"
}

class SquareBoard(
    var width: Int,
    var height: Int,
    var cellWidth: Float,
    var cellHeight: Float,
    var cellPadding: Float
) {
    var cells: ArrayList<Cell> = ArrayList()

    fun getCellOrNull(i: Int, j: Int): Cell? = cells.find { it.i == i && it.j == j }
    fun getCell(i: Int, j: Int): Cell = cells.first { it.i == i && it.j == j }
    fun getCellOrNullUnder(x: Float, y: Float): Cell? = cells.find {
        x >= (it.centerX - cellWidth / 2 + cellPadding.toInt() / 2)
                && x <= (it.centerX + cellWidth / 2 - cellPadding / 2)
                && y >= (it.centerY - cellHeight / 2 + cellPadding / 2)
                && y <= (it.centerY + cellHeight / 2 - cellPadding / 2)
    }

    init {
        for (i in 0 until height)
            for (j in 0 until width) {
                val centerX = j * cellWidth + cellWidth / 2 + cellPadding / 2
                val centerY = i * cellHeight + cellHeight / 2 + cellPadding / 2
                cells.add(Cell(i, j, centerX, centerY))
            }
    }

    fun calculateCenters() {
        for (i in 0 until height)
            for (j in 0 until width) {
                val centerX = j * cellWidth + cellWidth / 2 + cellPadding / 2
                val centerY = i * cellHeight + cellHeight / 2 + cellPadding / 2
                val cell = getCellOrNull(i, j)
                if (cell != null) {
                    cell.centerX = centerX
                    cell.centerY = centerY
                } else
                    cells.add(Cell(i, j, centerX, centerY))

            }
    }

}

class CanvasView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    private val sqaurePaint =
        Paint().apply {
            isAntiAlias = true
            color = Color.BLACK
            style = Paint.Style.STROKE
            strokeWidth = dpToPx(3f, context)
        }
    private val labelPaint =
        Paint().apply {
            isAntiAlias = true
            style = Paint.Style.FILL
            color = Color.BLACK
        }
    var rows: Int = 1
        set(value) {
            if (value > 0) {
                if (value == field)
                    return
                field = value
                calculateDimensions()
            }
        }
    var cols: Int = 1
        set(value) {
            if (value > 0) {
                if (value == field)
                    return
                field = value
                calculateDimensions()
            }
        }
    var cellsPadding = dpToPx(10f, context)
        set(value) {
            if (value == field)
                return
            field = value
            calculateDimensions()
        }

    var onCellClickListener: OnCellClickListner? = null
    var scaleType = ScaleType.CENTER_CROP
        set(value) {
            if (value == field)
                return
            field = value
            calculateDimensions()
        }
    private var cellWidth = 0f
    private var cellHeight = 0f
    private var squareBoard: SquareBoard =
        SquareBoard(cols, rows, cellWidth, cellHeight, cellsPadding)

    init {
        context.withStyledAttributes(attrs, R.styleable.CanvasView) {
            rows = getInteger(R.styleable.CanvasView_rows, 1)
            cols = getInteger(R.styleable.CanvasView_columns, 1)
            cellsPadding = dpToPx(
                getDimensionPixelSize(
                    R.styleable.CanvasView_cells_padding,
                    50
                ).toFloat(), context
            )
            sqaurePaint.apply {
                color = getColor(R.styleable.CanvasView_stroke_color, Color.BLACK)
                strokeWidth = getDimensionPixelSize(
                    R.styleable.CanvasView_stroke_width,
                    dpToPx(3f, context).toInt()
                ).toFloat()
                isAntiAlias = true
            }
            labelPaint.apply {
                color = getColor(R.styleable.CanvasView_text_color, Color.BLACK)
                textSize = getDimensionPixelSize(
                    R.styleable.CanvasView_text_size,
                    spToPx(18f, context).toInt()
                ).toFloat()
            }

            scaleType = ScaleType.values()[getInt(R.styleable.CanvasView_scale_type, 0)]

        }

        initSquareBoard()

    }

    private fun initSquareBoard() {
        squareBoard.let { board ->
            for (i in rows..board.height)
                for (j in cols..board.width)
                    board.cells = board.cells.filter { it.j != j && it.i != i } as ArrayList<Cell>

            board.width = cols
            board.height = rows
            board.cellWidth = cellWidth
            board.cellHeight = cellHeight
            board.cellPadding = cellsPadding
            board.calculateCenters()
        }
        //squareBoard = SquareBoard(cols, rows, cellWidth, cellHeight, cellsPadding)
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        calculateDimensions()
    }

    private fun calculateDimensions() {
        if (rows < 1 || cols < 1) {
            return
        }
        cellWidth = (width - cellsPadding) / cols
        cellHeight = (height - cellsPadding) / rows
        if (cellWidth <= cellsPadding || cellHeight <= cellsPadding)
            return
        initSquareBoard()
        for (cell in squareBoard.cells)
            cell.uri?.let {
                drawImagOnCellWithoutLoading(cell.i, cell.j)
            }
        invalidate()
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        canvas?.drawColor(Color.WHITE)
        if (rows == 0 || cols == 0 || cellWidth <= cellsPadding || cellHeight <= cellsPadding)
            return

        for (i in 0 until rows) {
            for (j in 0 until cols) {
                canvas?.save()

                canvas?.apply {

                    squareBoard.getCellOrNull(i, j)?.apply {
                        drawRect(
                            centerX - cellWidth / 2 + cellsPadding / 2,
                            centerY - cellHeight / 2 + cellsPadding / 2,
                            centerX + cellWidth / 2 - cellsPadding / 2,
                            centerY + cellHeight / 2 - cellsPadding / 2,
                            sqaurePaint
                        )

                        bitmap?.let {

                            drawBitmap(
                                it,
                                null,
                                calculateRect(
                                    centerX - it.width / 2,
                                    centerY - it.height / 2,
                                    centerX + it.width / 2,
                                    centerY + it.height / 2
                                ),
                                null
                            )
                        }
                    }
                }
                canvas?.restore()
            }
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(
            widthMeasureSpec,
            widthMeasureSpec
        )
    }

    private fun calculateRect(left: Float, top: Float, right: Float, bottom: Float): RectF {
        return RectF(left, top, right, bottom)
    }

    fun drawImagOnCell(uri: Uri, i: Int, j: Int) {
        drawImagOnCellWithoutInvalidate(uri, i, j)
        invalidate()

    }

    private fun drawImagOnCellWithoutInvalidate(uri: Uri, i: Int, j: Int) {
        squareBoard.getCellOrNull(i, j)?.apply {
            this.orgBitmap =
                decodeUriToBitmap(
                    context,
                    uri,
                    (cellWidth - cellsPadding).toInt(),
                    (cellHeight - cellsPadding).toInt()
                )
            this.bitmap = decodeUriToBitmap(
                orgBitmap,
                scaleType,
                (cellWidth - cellsPadding).toInt(),
                (cellHeight - cellsPadding).toInt()
            )
            this.uri = uri
        }

    }

    fun drawImagOnCellWithoutLoading(i: Int, j: Int) {
        squareBoard.getCellOrNull(i, j)?.apply {
            this.bitmap =
                decodeUriToBitmap(
                    orgBitmap,
                    scaleType,
                    (cellWidth - cellsPadding).toInt(),
                    (cellHeight - cellsPadding).toInt()
                )
            this.uri = uri
        }

    }

    fun drawImagOnCell(bitmap: Bitmap?, i: Int, j: Int) {
        squareBoard.getCellOrNull(i, j)?.apply {
            this.bitmap =
                bitmap
        }
        invalidate()

    }


    override fun onTouchEvent(event: MotionEvent?): Boolean {
        if (event?.action == MotionEvent.ACTION_DOWN) {
            val cell = event.let {
                squareBoard.getCellOrNullUnder(it.x, it.y)
            } ?: return false
            onCellClickListener?.onClick(cell)
            Toast.makeText(context, cell.toString(), Toast.LENGTH_SHORT).show()
            return true
        }
        return super.onTouchEvent(event)

    }


}