package com.example.inmaginassessment.utils

/**
 * Created by Raafat Alhmidi on 5/25/20 @10:19 AM.
 */
interface OnCellClickListner {

    fun onClick(cell:Cell?)
}