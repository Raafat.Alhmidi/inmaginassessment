package com.example.inmaginassessment.ui.filtersDialog

import android.content.Context
import android.graphics.Bitmap
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.inmaginassessment.R
import com.example.inmaginassessment.databinding.FilterItemLayoutBinding
import com.example.inmaginassessment.model.MyFilter
import jp.co.cyberagent.android.gpuimage.GPUImage

/**
 * Created by Raafat Alhmidi on 5/27/20 @8:28 AM.
 */
class FiltersAdapter(private val context:Context?) :
    RecyclerView.Adapter<MyViewHolder>() {



    var data: MutableList<MyFilter> = mutableListOf()
        set(value) {
            if (data.size == 0) {
                data.addAll(value)
                notifyItemRangeInserted(0, value.size)
            } else {
                val result = DiffUtil.calculateDiff(object : DiffUtil.Callback() {
                    override fun getOldListSize(): Int {
                        return data.size
                    }

                    override fun getNewListSize(): Int {
                        return value.size
                    }

                    override fun areItemsTheSame(
                        oldItemPosition: Int,
                        newItemPosition: Int
                    ): Boolean {
                        return data[oldItemPosition].equals(
                            value[newItemPosition]
                        )
                    }

                    override fun areContentsTheSame(
                        oldItemPosition: Int,
                        newItemPosition: Int
                    ): Boolean {
                        val newItem: MyFilter = value[newItemPosition]
                        val oldItem: MyFilter = data[oldItemPosition]
                        return newItem == oldItem
                    }
                })
                data = value
                result.dispatchUpdatesTo(this)
            }
        }
    var bitmap: Bitmap? = null


    override fun getItemCount(): Int = data.size


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val binding: FilterItemLayoutBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.filter_item_layout,
            parent,
            false
        )
        return MyViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(data[position], getBitmap(position))
    }

    private fun getBitmap(position:Int):Bitmap{
        val gpuImage = GPUImage(context)
        gpuImage.setFilter(data[position].gpuFilter)

        return gpuImage.getBitmapWithFilterApplied(bitmap)

    }

}

interface OnFilterClickListner {
    fun onClick(myFilter: MyFilter)
}

class MyViewHolder(private val binding: FilterItemLayoutBinding) :
    RecyclerView.ViewHolder(binding.root) {
    fun bind(myFilter: MyFilter, bitmap: Bitmap?) {
        binding.myFilter = myFilter
        binding.previewImgV.setImageBitmap(bitmap)

        binding.executePendingBindings()
    }


}
