package com.example.inmaginassessment.ui.filtersDialog

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider.NewInstanceFactory
import com.example.inmaginassessment.model.MyFilter
import java.util.*

/**
 * Created by Raafat Alhmidi on 5/27/20 @8:28 AM.
 */
class FiltersViewModel : ViewModel() {

    val selectedFilterLD = MutableLiveData<MyFilter>()

    companion object {
        var instance: FiltersViewModel? = null
            get() {
                if (field == null) field =
                    NewInstanceFactory().create(
                        FiltersViewModel::class.java
                    )
                return field
            }
            private set
    }

}
