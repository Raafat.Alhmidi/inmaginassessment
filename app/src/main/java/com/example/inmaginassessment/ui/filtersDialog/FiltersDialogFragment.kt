package com.example.inmaginassessment.ui.filtersDialog

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.Rect
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.*
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import androidx.annotation.DimenRes
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ItemDecoration
import com.example.inmaginassessment.R
import com.example.inmaginassessment.databinding.DialogApplyFilterBinding
import com.example.inmaginassessment.model.MyFilter
import com.example.inmaginassessment.utils.Cell
import com.example.inmaginassessment.utils.ScaleType
import com.example.inmaginassessment.decodeUriToBitmap
import jp.co.cyberagent.android.gpuimage.GPUImage
import jp.co.cyberagent.android.gpuimage.filter.*
import java.util.concurrent.Executors

/**
 * Created by Raafat Alhmidi on 5/27/20 @8:28 AM.
 */
class FiltersDialogFragment(var cell: Cell,val onDoneClickListner: OnDoneClickListner) : DialogFragment() {

    private lateinit var binding: DialogApplyFilterBinding
    private lateinit var adapter: FiltersAdapter
    private var prevSelectedFilter: MyFilter? = null
    private lateinit var progressDialog: AlertDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        adapter = FiltersAdapter(context)
        adapter.data = createFiltersList()
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog: Dialog = super.onCreateDialog(savedInstanceState)
        dialog.window?.requestFeature(Window.FEATURE_NO_TITLE)
        return dialog
    }

    override fun onStart() {
        super.onStart()
        val dialog = dialog
        if (dialog != null) {
            dialog.window
                ?.setLayout(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT
                )
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
//        dialog?.let {
//            it.window?.requestFeature(Window.FEATURE_NO_TITLE)
//            it.window?.setBackgroundDrawable(ColorDrawable(android.graphics.Color.TRANSPARENT))
//        }

        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_apply_filter, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        adapter.bitmap = cell.orgBitmap

        context?.let {
            HorizontalMarginItemDecoration(
                it, R.dimen.viewpager_current_item_horizontal_margin
            )
        }?.let {
            binding.dataVP.addItemDecoration(
                it
            )
        }
        binding.dataVP.offscreenPageLimit = 3

        val nextItemVisiblePx =
            resources.getDimensionPixelSize(R.dimen.viewpager_next_item_visible)
        val currentItemHorizontalMarginPx =
            resources.getDimensionPixelSize(R.dimen.viewpager_current_item_horizontal_margin)
        val pageTranslationX = nextItemVisiblePx + currentItemHorizontalMarginPx

        binding.dataVP.setPageTransformer { page, position ->
            page.translationX = -pageTranslationX * position
            page.scaleY = 1 - 0.25f * Math.abs(position)
        }
        binding.dataVP.adapter = adapter
        var idx = adapter.data.indexOf(cell.myFilter)
        if (idx < 0)
            idx = 0
        binding.dataVP.setCurrentItem(idx, true)

        binding.btnClose.setOnClickListener {
            dismiss()
        }
        binding.btnDone.setOnClickListener {
            setProgressDialog()
            Executors.newSingleThreadExecutor().execute {
                cell.bitmap = cell.bitmap?.let { it1 ->
                    getBitmap(
                        binding.dataVP.currentItem,
                        it1
                    )
                }
                cell.myFilter = adapter.data[binding.dataVP.currentItem]
                progressDialog.dismiss()
                onDoneClickListner.onClick(cell)
                dismiss()
            }
        }


    }

    fun getBitmap(position: Int, bitmap: Bitmap): Bitmap {
        val gpuImage = GPUImage(context)
        gpuImage.setFilter(adapter.data[position].gpuFilter)

        return gpuImage.getBitmapWithFilterApplied(bitmap)

    }

    private fun createFiltersList(): MutableList<MyFilter> {
        val res: MutableList<MyFilter> = mutableListOf()
        res.add(MyFilter(GPUImageFilter(), true, "None"))
        res.add(MyFilter(GPUImageEmbossFilter(), false, "Emboss"))
        res.add(MyFilter(GPUImageCrosshatchFilter(), false, "Crosshatch"))
        res.add(MyFilter(GPUImageHazeFilter(), false, "Haze"))
        res.add(MyFilter(GPUImageGrayscaleFilter(), false, "GrayScale"))
        res.add(MyFilter(GPUImageHalftoneFilter(), false, "HalfTone"))
        res.add(MyFilter(GPUImageDilationFilter(), false, "Dilation"))
        res.add(MyFilter(GPUImageKuwaharaFilter(), false, "Kuwahara"))
        res.add(MyFilter(GPUImageLuminanceFilter(), false, "Luminance"))
        res.add(MyFilter(GPUImagePixelationFilter(), false, "Pixelation"))
        res.add(MyFilter(GPUImageSketchFilter(), false, "Sketch"))
        res.add(MyFilter(GPUImageSwirlFilter(), false, "Swirl"))
        res.add(MyFilter(GPUImageVignetteFilter(), false, "Vignette"))
        res.add(MyFilter(GPUImageBilateralBlurFilter(), false, "BilateralBlur"))
        res.add(MyFilter(GPUImageGaussianBlurFilter(), false, "GaussianBlur"))
        res.add(MyFilter(GPUImageSolarizeFilter(), false, "Solarize"))
        res.add(MyFilter(GPUImageFalseColorFilter(), false, "FalseColor"))

        return res
    }


    internal class HorizontalMarginItemDecoration : ItemDecoration {
        private var horizontalMargin: Int

        constructor(context: Context, @DimenRes horizontalMarginInDp: Int) {
            horizontalMargin = context.resources.getDimensionPixelSize(horizontalMarginInDp)
        }

        constructor() {
            horizontalMargin = 10
        }

        override fun getItemOffsets(
            outRect: Rect,
            view: View,
            parent: RecyclerView,
            state: RecyclerView.State
        ) {
            outRect.right = horizontalMargin
            outRect.left = horizontalMargin
        }
    }

    fun setProgressDialog() {
        val ll = LinearLayout(context)
        ll.orientation = LinearLayout.HORIZONTAL
        ll.setPadding(30, 30, 30, 30)
        ll.gravity = Gravity.CENTER

        var llParam = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )

        llParam.gravity = Gravity.CENTER
        ll.layoutParams = llParam

        val progressBar = ProgressBar(context)
        progressBar.isIndeterminate = true
        progressBar.setPadding(0, 0, 39, 0)
        progressBar.layoutParams = llParam

        llParam = LinearLayout.LayoutParams(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        llParam.gravity = Gravity.CENTER
        val tvText = TextView(context)
        tvText.text = "Loading ..."
        tvText.setTextColor(Color.parseColor("#000000"))
        tvText.textSize = 20f
        tvText.layoutParams = llParam

        ll.addView(progressBar)
        ll.addView(tvText)

        val builder = AlertDialog.Builder(context);
        builder.setCancelable(true)
        builder.setView(ll)

        progressDialog = builder.create()
        progressDialog.show()
        val window = progressDialog.window
        if (window != null) {
            val layoutParams = WindowManager.LayoutParams()
            layoutParams.copyFrom(window.attributes)
            layoutParams.width = LinearLayout.LayoutParams.WRAP_CONTENT
            layoutParams.height = LinearLayout.LayoutParams.WRAP_CONTENT
            window.attributes = layoutParams
        }
    }
}
