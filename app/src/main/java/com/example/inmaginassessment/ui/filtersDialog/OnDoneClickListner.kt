package com.example.inmaginassessment.ui.filtersDialog

import com.example.inmaginassessment.utils.Cell

/**
 * Created by Raafat Alhmidi on 5/25/20 @10:19 AM.
 */
interface OnDoneClickListner {

    fun onClick(cell:Cell?)
}