package com.example.inmaginassessment.ui.settingsDialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import com.example.inmaginassessment.R
import com.example.inmaginassessment.databinding.SettingDialogLayoutBinding

/**
 * Created by Raafat Alhmidi on 5/27/20 @9:16 PM.
 */
class SettingsDialog : DialogFragment() {
    private lateinit var binding: SettingDialogLayoutBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.CustomDialog)

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.setting_dialog_layout, container, false);
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        SettingsViewModel.instance?.colsLD?.observe(this, Observer {
            binding.cols = it
        })

        SettingsViewModel.instance?.rowsLD?.observe(this, Observer {
            binding.rows = it
        })

        SettingsViewModel.instance?.paddingLD?.observe(this, Observer {
            binding.padding = it
        })

        SettingsViewModel.instance?.scaleTypeLD?.observe(this, Observer {
            binding.scaleType = it
        })

        binding.paddingSeekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                binding.padding = progress
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
            }

        })
        binding.widthSeekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                binding.cols = progress/10
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
            }

        })
        binding.heightSeekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                binding.rows = progress/10
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
            }

        })

        binding.okFab.setOnClickListener {
            SettingsViewModel.instance?.rowsLD?.value = binding.heightSeekBar.progress / 10
            SettingsViewModel.instance?.colsLD?.value = binding.widthSeekBar.progress / 10
            SettingsViewModel.instance?.paddingLD?.value = binding.paddingSeekBar.progress
            SettingsViewModel.instance?.scaleTypeLD?.value =
                binding.scaleTypeSpnr.selectedItemPosition
            dismiss()
        }

        binding.cancelFab.setOnClickListener {
            dismiss()
        }
    }

}