package com.example.inmaginassessment.ui.settingsDialog

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider.NewInstanceFactory

/**
 * Created by Raafat Alhmidi on 5/27/20 @8:28 AM.
 */
class SettingsViewModel : ViewModel() {

    val rowsLD = MutableLiveData<Int>()
    val colsLD = MutableLiveData<Int>()
    val paddingLD = MutableLiveData<Int>()
    val scaleTypeLD = MutableLiveData<Int>()

    companion object {
        var instance: SettingsViewModel? = null
            get() {
                if (field == null) field =
                    NewInstanceFactory().create(
                        SettingsViewModel::class.java
                    )
                return field
            }
            private set
    }

}
