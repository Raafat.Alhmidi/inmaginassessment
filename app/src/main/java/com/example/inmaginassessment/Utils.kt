package com.example.inmaginassessment

import android.R
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.RectF
import android.net.Uri
import android.util.DisplayMetrics
import android.util.TypedValue
import android.widget.Spinner
import androidx.databinding.BindingAdapter
import com.example.inmaginassessment.utils.ScaleType
import java.io.FileNotFoundException
import java.io.InputStream
import kotlin.math.min
import kotlin.math.roundToInt


/**
 * Created by Raafat Alhmidi on 5/22/20 @4:51 PM.
 */

@BindingAdapter("selectedPosition")
fun selectedPosition(view: Spinner, pos: Int) {
    view.setSelection(pos)
}

fun spToPx(sp: Float, context: Context): Float {
    return TypedValue.applyDimension(
        TypedValue.COMPLEX_UNIT_SP,
        sp,
        context.resources.displayMetrics
    )
}

fun dpToPx(dp: Float, context: Context): Float {
    return dp * (context.resources
        .displayMetrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)

}

fun pxToDp(px: Float, context: Context): Float {
    return px / (context.resources
        .displayMetrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)

}

fun dpToSp(dp: Float, context: Context): Float {
    return (dpToPx(dp, context) / context.resources
        .displayMetrics.scaledDensity)
}

fun calculateInSampleSize(options: BitmapFactory.Options, reqWidth: Int, reqHeight: Int): Int {
    // Raw height and width of image
    val (height: Int, width: Int) = options.run { outHeight to outWidth }
    var inSampleSize = 1

    if (height > reqHeight || width > reqWidth) {

        val halfHeight: Int = height / 2
        val halfWidth: Int = width / 2

        // Calculate the largest inSampleSize value that is a power of 2 and keeps both
        // height and width larger than the requested height and width.
        while (halfHeight / inSampleSize >= reqHeight && halfWidth / inSampleSize >= reqWidth) {
            inSampleSize *= 2
        }
    }

    return inSampleSize
}

fun decodeUriToBitmap(
    mContext: Context, sendUri: Uri?, destinationWidth: Int,
    destinationHeight: Int
): Bitmap? {
    var getBitmap: Bitmap? = null
    try {
        var imageStream: InputStream?
        try {
            var options = BitmapFactory.Options()
            options.inJustDecodeBounds = true

            imageStream = sendUri?.let { mContext.contentResolver.openInputStream(it) }
            BitmapFactory.decodeStream(imageStream, null, options)
            imageStream?.close()

            var imageHeight: Int = options.outHeight
            var imageWidth: Int = options.outWidth
            val aspectRatio = imageWidth.toFloat() / imageHeight

            if (imageWidth < imageHeight) {
                imageWidth = destinationWidth
                imageHeight = (imageWidth / aspectRatio).toInt()
            } else {
                imageHeight = destinationHeight
                imageWidth = (imageHeight * aspectRatio).toInt()
            }

            options.inSampleSize = calculateInSampleSize(options, imageWidth, imageHeight)
            options.inJustDecodeBounds = false
            imageStream = sendUri?.let { mContext.contentResolver.openInputStream(it) }
            getBitmap = BitmapFactory.decodeStream(imageStream, null, options)
            imageStream?.close()


        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        }
    } catch (e: Exception) {
        e.printStackTrace()
    }
    return getBitmap
}

fun decodeUriToBitmap(
    srcBitmap: Bitmap?,
    scaleType: ScaleType,
    destinationWidth: Int,
    destinationHeight: Int
): Bitmap? {

    var width = srcBitmap?.width?.let {
        dpToPx(
            it.toFloat(),
            MyApplication.instance
        ).toInt()
    } ?: 0
    var height =
        srcBitmap?.height?.let {
            dpToPx(
                it.toFloat(),
                MyApplication.instance
            ).toInt()
        } ?: 0

    val aspectRatio = srcBitmap?.let {
        it.width.toFloat() / it.height
    } ?: 0f
    var resBitmap: Bitmap? = when (scaleType) {
        ScaleType.CENTER -> {

            srcBitmap?.let {
                cropBitmap(
                    it,
                    min(height, destinationHeight),
                    min(width, destinationWidth)
                )
            }
        }
        ScaleType.CENTER_CROP -> {
            srcBitmap?.let {
                scaleCenterCrop(
                    it,
                    destinationWidth,
                    destinationHeight
                )
            }

        }
        ScaleType.CENTER_INSIDE -> {
            if (width * destinationHeight > height * destinationWidth) {
                width = destinationWidth
                height = (width / aspectRatio).toInt()
            } else {
                height = destinationHeight
                width = (height * aspectRatio).toInt()
            }

            width = if (width <= 0) 1 else width
            height = if (height <= 0) 1 else height

            srcBitmap?.let {
                scaleCenterCrop(
                    it,
                    width,
                    height
                )
            }

        }
        ScaleType.FIT_END -> {
            srcBitmap?.let {
                fitEnd(
                    it,
                    destinationWidth,
                    destinationHeight
                )
            }

        }
        ScaleType.FIT_START -> {

            srcBitmap?.let {
                fitStart(
                    it,
                    destinationWidth,
                    destinationHeight
                )
            }
        }
        ScaleType.FIT_XY -> {
            (srcBitmap?.let {
                Bitmap.createScaledBitmap(
                    it,
                    destinationWidth,
                    destinationHeight,
                    false
                )
            })

        }
    }
    return resBitmap
}

fun decodeUriToBitmap(
    mContext: Context,
    sendUri: Uri?,
    scaleType: ScaleType,
    destinationWidth: Int,
    destinationHeight: Int
): Bitmap? {
    val srcBitmap =
        decodeUriToBitmap(mContext, sendUri, destinationWidth, destinationHeight)
    var width = srcBitmap?.width?.let {
        dpToPx(
            it.toFloat(),
            MyApplication.instance
        ).toInt()
    } ?: 0
    var height =
        srcBitmap?.height?.let {
            dpToPx(
                it.toFloat(),
                MyApplication.instance
            ).toInt()
        } ?: 0

    val aspectRatio = srcBitmap?.let {
        it.width.toFloat() / it.height
    } ?: 0f
    var resBitmap: Bitmap? = when (scaleType) {
        ScaleType.CENTER -> {

            srcBitmap?.let {
                cropBitmap(
                    it,
                    min(height, destinationHeight),
                    min(width, destinationWidth)
                )
            }
        }
        ScaleType.CENTER_CROP -> {
            srcBitmap?.let {
                scaleCenterCrop(
                    it,
                    destinationWidth,
                    destinationHeight
                )
            }

        }
        ScaleType.CENTER_INSIDE -> {
            if (width * destinationHeight > height * destinationWidth) {
                width = destinationWidth
                height = (width / aspectRatio).toInt()
            } else {
                height = destinationHeight
                width = (height * aspectRatio).toInt()
            }

            width = if (width <= 0) 1 else width
            height = if (height <= 0) 1 else height

            srcBitmap?.let {
                scaleCenterCrop(
                    it,
                    width,
                    height
                )
            }

        }
        ScaleType.FIT_END -> {
            srcBitmap?.let {
                fitEnd(
                    it,
                    destinationWidth,
                    destinationHeight
                )
            }

        }
        ScaleType.FIT_START -> {

            srcBitmap?.let {
                fitStart(
                    it,
                    destinationWidth,
                    destinationHeight
                )
            }
        }
        ScaleType.FIT_XY -> {
            (srcBitmap?.let {
                Bitmap.createScaledBitmap(
                    it,
                    destinationWidth,
                    destinationHeight,
                    false
                )
            })

        }
    }
    return resBitmap
}

fun scaleCenterCrop(source: Bitmap, viewWidth: Int, viewHeight: Int): Bitmap? {

    val sourceWidth = source.width
    val sourceHeight = source.height

    val xScale = viewWidth.toFloat() / sourceWidth
    val yScale = viewHeight.toFloat() / sourceHeight
    val scale = xScale.coerceAtLeast(yScale)

    val scaledWidth = scale * sourceWidth
    val scaledHeight = scale * sourceHeight

    val left = (viewWidth - scaledWidth) / 2
    val top = (viewHeight - scaledHeight) / 2

    val targetRect = RectF(left, top, left + scaledWidth, top + scaledHeight)
    val dest = Bitmap.createBitmap(viewWidth, viewHeight, Bitmap.Config.ARGB_8888)
    val canvas = Canvas(dest)
    canvas.drawBitmap(source, null, targetRect, null)
    return dest
}

fun cropBitmap(source: Bitmap, viewWidth: Int, viewHeight: Int): Bitmap? {
    val sourceWidth = dpToPx(
        source.width.toFloat(),
        MyApplication.instance
    )
    val sourceHeight = dpToPx(
        source.height.toFloat(),
        MyApplication.instance
    )

    val left = (viewWidth - sourceWidth) / 2f
    val top = (viewHeight - sourceHeight) / 2f

    val targetRect = RectF(left, top, left + sourceWidth, top + sourceHeight)

    val dest = Bitmap.createBitmap(
        viewWidth,
        viewHeight,
        Bitmap.Config.ARGB_8888
    )
    val canvas = Canvas(dest)
    canvas.drawBitmap(source, null, targetRect, null)

    return dest
}

fun fitStart(source: Bitmap, viewWidth: Int, viewHeight: Int): Bitmap? {
    val sourceWidth = dpToPx(
        source.width.toFloat(),
        MyApplication.instance
    )
    val sourceHeight = dpToPx(
        source.height.toFloat(),
        MyApplication.instance
    )

    val aspectRatio = sourceWidth / sourceHeight
    var newWidth = viewWidth.toFloat()
    var newHeight = newWidth / aspectRatio

    if (newHeight >= viewHeight) {
        newHeight = viewHeight.toFloat()
        newWidth = newHeight * aspectRatio
    }

    val left = 0f
    val top = 0f

    val targetRect = RectF(left, top, left + newWidth, top + newHeight)

    val dest = Bitmap.createBitmap(
        viewWidth,
        viewHeight,
        Bitmap.Config.ARGB_8888
    )
    val canvas = Canvas(dest)
    canvas.drawBitmap(source, null, targetRect, null)
    return dest
}

fun fitEnd(source: Bitmap, viewWidth: Int, viewHeight: Int): Bitmap? {
    val sourceWidth = dpToPx(
        source.width.toFloat(),
        MyApplication.instance
    )
    val sourceHeight = dpToPx(
        source.height.toFloat(),
        MyApplication.instance
    )

    val aspectRatio = sourceWidth / sourceHeight
    var newWidth = viewWidth.toFloat()
    var newHeight = newWidth / aspectRatio

    if (newHeight >= viewHeight) {
        newHeight = viewHeight.toFloat()
        newWidth = newHeight * aspectRatio
    }

    val left = viewWidth - newWidth
    val top = viewHeight - newHeight

    val targetRect = RectF(left, top, left + newWidth, top + newHeight)

    val dest = Bitmap.createBitmap(
        viewWidth,
        viewHeight,
        Bitmap.Config.ARGB_8888
    )
    val canvas = Canvas(dest)
    canvas.drawBitmap(source, null, targetRect, null)
    return dest
}


fun calculateNewSize(
    srcWidth: Float,
    srcHeight: Float,
    desWidth: Float,
    desHeight: Float,
    scaleType: ScaleType
): Pair<Float, Float> {
    val aspectRatio = srcWidth / srcHeight
    return when (scaleType) {
        ScaleType.CENTER -> {
            srcWidth to srcHeight
        }
        ScaleType.CENTER_CROP -> {
            if (desWidth * srcHeight > desHeight * srcWidth) {
                val width = desWidth
                val height = (width / aspectRatio)
                width to height
            } else {
                val height = desHeight
                val width = (height * aspectRatio)

                width to height
            }

        }
        ScaleType.CENTER_INSIDE -> {
            srcWidth to srcHeight
        }
        ScaleType.FIT_END -> {
            srcWidth to srcHeight

        }
        ScaleType.FIT_START -> {
            srcWidth to srcHeight
        }
        ScaleType.FIT_XY -> {
            desWidth to desHeight
        }
    }
}