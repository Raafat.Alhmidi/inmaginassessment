package com.example.inmaginassessment.model

import jp.co.cyberagent.android.gpuimage.filter.GPUImageFilter

/**
 * Created by Raafat Alhmidi on 5/27/20 @8:24 AM.
 */
data class MyFilter(var gpuFilter:GPUImageFilter?,var selected:Boolean,var name:String) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as MyFilter

        if (name != other.name) return false

        return true
    }

    override fun hashCode(): Int {
        var result = gpuFilter?.hashCode() ?: 0
        result = 31 * result + selected.hashCode()
        result = 31 * result + name.hashCode()
        return result
    }
}